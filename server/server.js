import express from "express";
import bodyParser from "body-parser";
import config from "../config/config";
import init_db_connection from "./db/init-mongoose";
import { TodosRoutes, UsersRoutes } from "./routes";

const port = process.env.PORT || config.app.port || 3000;

const app = new express();
app.use(bodyParser.json());
app.use(`/api/${config.app.version}`, [UsersRoutes, TodosRoutes]);
// app.use("/api/v1/todos", TodosRoutes);

app.listen(port, () => {
  init_db_connection(() => console.log(`Starting server on localhost:${port}`));
});

export default app;
