import { ObjectID } from "mongodb";
import _ from "lodash";

import { Todo } from "../models";

const getTodos = (req, res, next) => {
  Todo.find().then(
    (todos) => res.send({ todos }),
    (err) => res.status(400).send(err)
  );
};

const createTodo = (req, res, next) => {
  new Todo({
    text: req.body.text,
  })
    .save()
    .then(
      (doc) => res.send(doc),
      (err) => res.status(400).send(err)
    );
};

const findTodo = (req, res, next) => {
  const id = req.params.id;
  if (!ObjectID.isValid(id)) {
    console.log(`ID=${id} is not valid.`);
    return res.status(404).send({ message: `ID=${id} is not valid.` });
  }
  Todo.findById(id).then(
    (todo) => {
      if (!todo) {
        return res.status(404).send({ message: `No todo found with id=${id}` });
      } else {
        return res.status(200).send(todo);
      }
    },
    (err) => {
      return res.status(400).send({ message: err.message });
    }
  );
};

const deleteTodo = (req, res, next) => {
  const id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send({ message: `ID=${id} is not valid.` });
  }
  Todo.findByIdAndRemove(id).then(
    (todo) => {
      if (!todo) {
        return res.status(404).send({ message: `No todo found with id=${id}` });
      }
      return res.status(200).send(todo);
    },
    (err) => {
      return res.status(400).send({ message: err.message });
    }
  );
};

const updateTodo = (req, res, next) => {
  const id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send({ message: `ID=${id} is not valid.` });
  }
  const body = _.pick(req.body, ["text", "completed"]);
  if (_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime();
  } else {
    body.completed = false;
    body.completedAt = null;
  }

  Todo.findByIdAndUpdate(id, { $set: body }, { new: true }).then(
    (todo) => {
      if (!todo) {
        return res.status(404).send({ message: `No todo found with id=${id}` });
      } else {
        return res.status(200).send(todo);
      }
    },
    (err) => {
      return res.status(400).send({ message: err.message });
    }
  );
};

export { getTodos, createTodo, findTodo, deleteTodo, updateTodo };
