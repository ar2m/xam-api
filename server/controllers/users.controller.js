import _ from "lodash";

import { User } from "../models";

const getUsers = (req, res, next) => {
  User.find().then(
    (users) => res.send({ users }),
    (err) => res.status(400).send(err)
  );
};

const getMe = (req, res, next) => {
  res.send(req.user);
};

const createUser = (req, res, next) => {
  const body = _.pick(req.body, ["email", "password"]);
  const user = new User(body);
  user
    .save()
    .then(
      () => {
        return user.generateAuthToken();
      },
      (err) => res.status(400).send({ message: err.message })
    )
    .then((token) => res.header("x-auth", token).status(200).send(user));
};

export { getUsers, getMe, createUser };
