import express from "express";

import {
  getTodos,
  createTodo,
  findTodo,
  deleteTodo,
  updateTodo,
} from "../controllers/todos.controller";

const TodosRouter = express.Router();
TodosRouter.use("/todos", TodosRouter);

TodosRouter.post("/", (req, res) => {
  createTodo(req, res);
});

TodosRouter.get("/", (req, res) => {
  getTodos(req, res);
});

TodosRouter.get("/:id", (req, res) => {
  findTodo(req, res);
});

TodosRouter.delete("/:id", (req, res) => {
  deleteTodo(req, res);
});

TodosRouter.patch("/:id", (req, res) => {
  updateTodo(req, res);
});

export default TodosRouter;
