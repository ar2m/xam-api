import express from "express";

import { authenticate } from "../middleware/authenticate";
import { getUsers, getMe, createUser } from "../controllers/users.controller";

const UsersRouter = express.Router();
UsersRouter.use("/users", UsersRouter);

UsersRouter.post("/", (req, res) => {
  createUser(req, res);
});

UsersRouter.get("/", (req, res) => {
  getUsers(res, res);
});

UsersRouter.get("/me", authenticate, (req, res) => {
  getMe(req, res);
});

export default UsersRouter;
