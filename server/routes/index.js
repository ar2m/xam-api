import UsersRoutes from "./users.routes";
import TodosRoutes from "./todos.routes";

export { TodosRoutes, UsersRoutes };
