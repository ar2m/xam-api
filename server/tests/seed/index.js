import { ObjectID } from "mongodb";
import jwt from "jsonwebtoken";

import { User, Todo } from "../../models";

const todos = [
  {
    _id: new ObjectID(),
    text: "Simple todo 11",
  },
  {
    _id: new ObjectID(),
    text: "Simple todo 22",
    completed: true,
    completedAt: 333,
  },
];

const userOneId = new ObjectID();
const userTwoId = new ObjectID();

const users = [
  {
    _id: userOneId,
    email: "mhamidi@yopmail.com",
    password: "mhamidiPass",
    tokens: [
      {
        access: "auth",
        token: jwt
          .sign({ _id: userOneId, access: "auth" }, "abc123")
          .toString(),
      },
    ],
  },
  {
    _id: userTwoId,
    email: "ihamidi@yopmail.com",
    password: "ihamidiPass",
  },
];

const populateTodos = (done) => {
  Todo.deleteMany({})
    .then(() => Todo.insertMany(todos))
    .then(() => done());
};

const populateUsers = (done) => {
  User.deleteMany({})
    .then(() => {
      const userOne = new User(users[0]).save();
      const userTwo = new User(users[1]).save();

      return Promise.all([userOne, userTwo]);
    })
    .then(() => done());
};

export { todos, populateTodos, users, populateUsers };
