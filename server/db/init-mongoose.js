import mongoose from "mongoose";
import config from "../../config/config";

const dbName = config.db.name;

let dbUrl = `${config.db.protocol}://${config.db.username}:${config.db.password}@${config.db.host}`;
if (config.db.port) {
  dbUrl += `:${config.db.port}/${dbName}`;
} else {
  dbUrl += `${dbName}`;
}

// mongodb atlas cluster
// "mongodb+srv://aiaexpert:aiaexpert00@mongodb-atlas-training-zmpo1.mongodb.net/test?retryWrites=true"
mongoose.Promise = global.Promise;
const init_db_connection = (callback) =>
  mongoose
    .connect(dbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .then(
      (success) => {
        callback();
        console.log("connection done..", dbUrl);
      },
      (error) => console.log("connection err..", dbUrl, error)
    );

export default init_db_connection;
